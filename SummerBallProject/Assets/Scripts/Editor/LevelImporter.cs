﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class LevelImporter : AssetPostprocessor{

	private const int textureSize = 128;

	private void OnPreprocessTexture(){

		var fileNameIndex = assetPath.LastIndexOf ('/');
		var fileName = assetPath.Substring (fileNameIndex + 1);

		if (!fileName.Contains ("Level"))
			return;

		var importer = assetImporter as TextureImporter;

		importer.isReadable = true;
		importer.mipmapEnabled = false;
		importer.wrapMode = TextureWrapMode.Clamp;
		importer.filterMode = FilterMode.Trilinear;
		importer.compressionQuality = 0;
		importer.textureCompression = TextureImporterCompression.Uncompressed;
		if (importer.maxTextureSize > textureSize) {
			importer.maxTextureSize = textureSize;
		}
		importer.npotScale = TextureImporterNPOTScale.None;

	}

}

