﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ColliderGenerator))]
public class LevelGeneratorEditor : Editor {

	public override void OnInspectorGUI(){
		DrawDefaultInspector ();

		if(GUILayout.Button ("Generate", GUILayout.Height (40))){
			var trg = (ColliderGenerator)target;

			trg.GenerateLevel ();
		}
		if(GUILayout.Button ("Generate Colliders", GUILayout.Height (40))){
			var trg = (ColliderGenerator)target;

			trg.GenerateColliders ();
		}
		if(GUILayout.Button ("Set Colliders", GUILayout.Height (40))){
			var trg = (ColliderGenerator)target;

			trg.AttachColliders ();
		}
		if(GUILayout.Button ("Clean", GUILayout.Height (40))){
			var trg = (ColliderGenerator)target;

			trg.Clean();
		}
	}
}

