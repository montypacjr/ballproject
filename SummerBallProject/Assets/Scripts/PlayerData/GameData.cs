using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


[CreateAssetMenu()]
public class GameData : ScriptableObject
{

	public SkinList baseS,normalS,rareS,trailS;

	[SerializeField]
	int totalRings;
	[SerializeField]
	int[] JumpsRings;
	[SerializeField]
	int[] FastRings;
	[SerializeField]
	int[] RevertRings;
	[SerializeField]
	int[] StopRings;
	[SerializeField]
	int skin;
	[SerializeField]
	int trailSkin;
	[SerializeField]
	int trailColor;
	[SerializeField]
	int levelsUnlocked;
	[SerializeField]
	bool[] baseSkins;
	[SerializeField]
	bool[] normalSkins;
	[SerializeField]
	bool[] rareSkins;
	[SerializeField]
	bool[] trailSkins;

	string fileName = "/playerData.txt";

	public int TotalRings {
		get {
			return this.totalRings;
		}
		private set {}
	}

	public int Skin {
		get {
			return this.skin;
		}
		set {
			skin = value;
		}
	}

	public int TrailSkin {
		get {
			return this.trailSkin;
		}
		set {
			trailSkin = value;
		}
	}

	public int TrailColor {
		get {
			return this.trailColor;
		}
		set {
			trailColor = value;
		}
	}

	public int LevelsUnlocked {
		get{
			return this.levelsUnlocked;
		}
		set {
			levelsUnlocked = value;
		}
	}
	public int TotalJumps{
		get{
			int sum = 0;
			for (int i = 0; i < 10; i++) {
				sum += JumpsRings [i];
			}
			return sum;
		}
		private set {}
	}
	public int TotalFast{
		get{
			int sum = 0;
			for (int i = 0; i < 10; i++) {
				sum += FastRings [i];
			}
			return sum;
		}
		private set {}
	}
	public int TotalRevert{
		get{
			int sum = 0;
			for (int i = 0; i < 10; i++) {
				sum += RevertRings [i];
			}
			return sum;
		}
		private set {}
	}
	public int TotalStop{
		get{
			int sum = 0;
			for (int i = 0; i < 10; i++) {
				sum += StopRings [i];
			}
			return sum;
		}
		private set {}
	}

	public void SetLevelRings(int zone, int level, int ringNumber){

		switch (zone) {
		case 0:
			if (JumpsRings [level] < ringNumber) {
				JumpsRings [level] = ringNumber;
				totalRings += ringNumber;
			}
			break;
		case 1:
			if (FastRings [level] < ringNumber) {
				FastRings [level] = ringNumber;
				totalRings += ringNumber;
			}
			break;
		case 2:
			if (RevertRings [level] < ringNumber) {
				RevertRings [level] = ringNumber;
				totalRings += ringNumber;
			}
			break;
		case 3:
			if (StopRings [level] < ringNumber) {
				StopRings [level] = ringNumber;
				totalRings += ringNumber;
			}
			break;
		default:
			break;
		}

	}

	public int GetLevelRings(int zone, int level){
		switch (zone) {
		case 0:
			return JumpsRings[level];
		case 1:
			return FastRings[level];
		case 2:
			return RevertRings[level];
		case 3:
			return StopRings[level];
		default:
			return 0;
		}
	}

	public void Save(){
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file;
		if (Application.isEditor)
			file = File.Open (Application.dataPath + fileName,FileMode.Open);
		else
			file = File.Open (Application.persistentDataPath + fileName,FileMode.Open);
		
		PlayerData data = new PlayerData ();
		InitialUnlocked ();

		data.JumpsRings = JumpsRings;
		data.AccelDeccelRings = FastRings;
		data.ChangeRings = RevertRings;
		data.StopRings = StopRings;
		data.totalRings = totalRings;
		data.skin = skin;
		data.trailSkin = trailSkin;
		data.trailColor = trailColor;
		data.levelsUnlocked = levelsUnlocked;
		data.baseSkins = baseSkins;
		data.normalSkins = normalSkins;
		data.rareSkins = rareSkins;
		data.trailSkins = trailSkins;

		bf.Serialize (file,data);

		file.Close ();
		Debug.Log ("DataSaved");
	}

	public void Load(){
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file;
		PlayerData data;
		if (Application.isEditor) {
			
			if (File.Exists (Application.dataPath + fileName)) {
				file = File.Open (Application.dataPath + fileName, FileMode.Open);
				data = (PlayerData)bf.Deserialize (file);
				if (Application.version == data.version) {
					levelsUnlocked = data.levelsUnlocked;
				}				
				totalRings = data.totalRings;
				JumpsRings = data.JumpsRings;
				FastRings = data.AccelDeccelRings;
				RevertRings = data.ChangeRings;
				StopRings = data.StopRings;
				skin = data.skin;
				trailSkin = data.trailSkin;
				trailColor = data.trailColor;
				baseSkins = data.baseSkins;
				normalSkins = data.normalSkins;
				rareSkins = data.rareSkins;
				trailSkins = data.trailSkins;
				SetUnlocked ();

			} else {
				file = File.Create (Application.dataPath + fileName);
				data = new PlayerData ();
				InitialValues ();
				InitialUnlocked ();
				data.JumpsRings = JumpsRings;
				data.AccelDeccelRings = FastRings;
				data.ChangeRings = RevertRings;
				data.StopRings = StopRings;
				data.totalRings = totalRings;
				data.skin = skin;
				data.trailSkin = trailSkin;
				data.trailColor = trailColor;
				data.levelsUnlocked = levelsUnlocked;
				data.version = Application.version;
				data.baseSkins = baseSkins;
				data.normalSkins = normalSkins;
				data.rareSkins = rareSkins;
				data.trailSkins = trailSkins;
				bf.Serialize (file, data);
			}
		}else if (Debug.isDebugBuild) {
			if (File.Exists (Application.persistentDataPath + fileName)) {
				file = File.Open (Application.persistentDataPath + fileName, FileMode.Open);
				data = (PlayerData)bf.Deserialize (file);
				if (Application.version == data.version) {
					levelsUnlocked = data.levelsUnlocked;
				}
				totalRings = data.totalRings;
				JumpsRings = data.JumpsRings;
				FastRings = data.AccelDeccelRings;
				RevertRings = data.ChangeRings;
				StopRings = data.StopRings;
				skin = data.skin;
				trailSkin = data.trailSkin;
				trailColor = data.trailColor;
				levelsUnlocked = data.levelsUnlocked;
				baseSkins = data.baseSkins;
				normalSkins = data.normalSkins;
				rareSkins = data.rareSkins;
				trailSkins = data.trailSkins;
				SetUnlocked ();
			} else {
				file = File.Create (Application.persistentDataPath + fileName);
				data = new PlayerData ();
				InitialValues ();
				InitialUnlocked ();
				data.JumpsRings = JumpsRings;
				data.AccelDeccelRings = FastRings;
				data.ChangeRings = RevertRings;
				data.StopRings = StopRings;
				data.totalRings = totalRings;
				data.skin = skin;
				data.trailSkin = trailSkin;
				data.trailColor = trailColor;
				data.levelsUnlocked = levelsUnlocked;
				data.baseSkins = baseSkins;
				data.normalSkins = normalSkins;
				data.rareSkins = rareSkins;
				data.trailSkins = trailSkins;
				bf.Serialize (file, data);
			}
		} 
		else {
			if (File.Exists (Application.persistentDataPath + fileName)) {
				file = File.Open (Application.persistentDataPath + fileName, FileMode.Open);
				data = (PlayerData)bf.Deserialize (file);
				totalRings = data.totalRings;
				JumpsRings = data.JumpsRings;
				FastRings = data.AccelDeccelRings;
				RevertRings = data.ChangeRings;
				StopRings = data.StopRings;
				skin = data.skin;
				trailSkin = data.trailSkin;
				trailColor = data.trailColor;
				levelsUnlocked = data.levelsUnlocked;
				baseSkins = data.baseSkins;
				normalSkins = data.normalSkins;
				rareSkins = data.rareSkins;
				trailSkins = data.trailSkins;
				SetUnlocked ();
			} else {
				file = File.Create (Application.persistentDataPath + fileName);
				data = new PlayerData ();
				InitialValues ();
				InitialUnlocked ();
				data.JumpsRings = JumpsRings;
				data.AccelDeccelRings = FastRings;
				data.ChangeRings = RevertRings;
				data.StopRings = StopRings;
				data.totalRings = totalRings;
				data.skin = skin;
				data.trailSkin = trailSkin;
				data.trailColor = trailColor;
				data.levelsUnlocked = levelsUnlocked;
				data.baseSkins = baseSkins;
				data.normalSkins = normalSkins;
				data.rareSkins = rareSkins;
				data.trailSkins = trailSkins;
				bf.Serialize (file, data);
			}
		}
		file.Close ();
		Debug.Log ("DataLoaded");
	}

	void InitialValues(){
		totalRings = 0;
		skin = 0;
		trailSkin = 0;
		trailColor = 0;
		levelsUnlocked = 1;
		JumpsRings = new int[10];
		FastRings = new int[10];
		RevertRings = new int[10];
		StopRings = new int[10];
		baseSkins = new bool[9];
		normalSkins = new bool[11];
		rareSkins = new bool[10];
		trailSkins = new bool[5];
	}
	void InitialUnlocked(){
		baseSkins = baseS.GetUnlocked ();
		normalSkins = normalS.GetUnlocked ();
		rareSkins = rareS.GetUnlocked ();
		trailSkins = trailS.GetUnlocked ();
	}
	void SetUnlocked(){
		baseS.SetUnlocked (baseSkins);
		normalS.SetUnlocked (normalSkins);
		rareS.SetUnlocked (rareSkins);
		trailS.SetUnlocked (trailSkins);
	}
}
[System.Serializable]
class PlayerData{
	
	public int totalRings;
	public int[] JumpsRings;
	public int[] AccelDeccelRings;
	public int[] ChangeRings;
	public int[] StopRings;
	public int skin;
	public int trailSkin;
	public int trailColor;
	public int levelsUnlocked;
	public string version;
	public bool[] baseSkins;
	public bool[] normalSkins;
	public bool[] rareSkins;
	public bool[] trailSkins;
}


