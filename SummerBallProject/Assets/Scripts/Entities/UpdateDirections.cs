﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateDirections : MonoBehaviour {

	Animator anim;

	void Awake(){
		anim = GetComponent <Animator>();
	}

	void OnEnable(){
		PlayerController.left += UpdateDirection;
		UpdateDirection (false);
	}

	void OnDisable () {
		PlayerController.left -= UpdateDirection;
	}

	public void UpdateDirection (bool state){

		if (!state) {
			anim.SetTrigger (StaticStrings.Parameters.rightParam);
		}else{
			anim.SetTrigger (StaticStrings.Parameters.leftParam);
		}
			
	}
		
}
