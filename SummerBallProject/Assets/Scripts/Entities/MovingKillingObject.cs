﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingKillingObject : MonoBehaviour {

	public Transform[] movingPoints;
	Transform nextPoint;
	int pointsIterator;
	public float movingSpeed;
	public Transform enemy;

	void OnEnable(){
		enemy.position = movingPoints [0].position;
	}
	// Use this for initialization
	void Start () {
		pointsIterator = 1;
		nextPoint = movingPoints [pointsIterator];
	}

	// Update is called once per frame
	void Update () {

		enemy.position = Vector3.MoveTowards (enemy.position, nextPoint.position, movingSpeed * Time.deltaTime);

		if(enemy.position == nextPoint.position){
			pointsIterator++;
			if(pointsIterator == movingPoints.Length){
				pointsIterator = 0;
			}
			nextPoint = movingPoints [pointsIterator];
		}

	}
}
