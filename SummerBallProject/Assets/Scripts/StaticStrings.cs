﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StaticStrings{
	public class Tags{
		public static string playerTag = "Player";
		public static string floorTag = "Floor";
		public static string deathTag = "Death";
		public static string killingObjectTag = "KillingObject";
		public static string accelerationPadTag = "AccelerationPad";
		public static string nextLevelPadTag = "NextLevelPad";
		public static string upperLevelPad = "UpperLevelPad";
		public static string changeDirectionPadTag = "ChangeDirectionPad";
		public static string breakPadTag = "BreakPad";
		public static string stopPadTag = "StopPad";
		public static string doubleJumpTag = "DoubleJump";
		public static string ringTag = "Ring";
		public static string wallTag = "Wall";
	}
	public class Keys{
		public static string skinKey = "Skin";
		public static string totalRingKey = "TotalRing";
		public static string colorKey = "_RimColor";
	}
	public class Parameters{
		public static string leftParam = "Left";
		public static string rightParam = "Right";
	}
}
