﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkinMenu : SimpleMenu<SkinMenu> {

	public Button button;
	public Image buttonImage;
	public Text text;
	public Sprite videoImage, lockedImage;

	void OnEnable(){
		text.text = "u";
	}

	public void OnRightPressed(){
		//next skin
		GameManager.Instance.selector.NextSkin ();
	}
	public void OnLeftPressed(){
		//previous skin
		GameManager.Instance.selector.PreviousSkin ();
	}
	public void OnSelectPressed(){
		//selct skin
		GameManager.Instance.selector.SelectSkin ();
		OnUnlockPressed ();
	}
	public void OnUnlockPressed(){
		if (!text.text.Contains ("ad")) {
			if (text.text.Contains ("u")) {
				Hide ();
				GameManager.Instance.data.Save ();
			} else if (!text.text.Contains ("Lvl")) {
				AcceptUnlockMenu.Show ();
			}
			else {
				UnlockByLevelMenu.Show ();
			}
		}else{
			//play ad
			AdsManager.Instance.ShowRewardedAd ();
			Debug.Log ("Watch Ad");
		}
	}
	public void SetButtonText(string text){
		if (text.Contains ("u")) {
			button.gameObject.SetActive (false);
			this.text.text = text;
		}else if (text.Contains ("Lvl")) {
			button.gameObject.SetActive (true);
			button.interactable = false;
			buttonImage.sprite = lockedImage;
			this.text.gameObject.SetActive (true);
			this.text.text = text;
		}else if (text.Contains ("ad")) {
			button.gameObject.SetActive (true);
			button.interactable = true;
			this.text.gameObject.SetActive (false);
			buttonImage.sprite = videoImage;
			this.text.text = text;
		}else{
			button.gameObject.SetActive (true);
			button.interactable = true;
			buttonImage.sprite = lockedImage;
			this.text.gameObject.SetActive (true);
			this.text.text = text;
		}

	}
	public override void OnBackPressed ()
	{
		GameManager.Instance.selector.DisableAll ();
		Hide ();
	}
}
