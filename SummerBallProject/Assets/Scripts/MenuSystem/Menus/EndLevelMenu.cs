﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndLevelMenu : Menu<EndLevelMenu> {

	public Text levelRings;

	string lineEnd = "/6";
	static int rings;

	void OnEnable(){
		this.levelRings.text = rings.ToString () + lineEnd;
	}

	public static void Show(int levelRings)
	{
		rings = levelRings;
		Open();
		LevelSelectorManager.Instance.Disable ();
		SkinManager.Instance.CheckForSkinUnlock (LevelSelectorManager.Instance.GetLevel ());
	}

	public static void Hide()
	{
		Close();
	}

	public void OnMainMenuPressed()
	{
		Hide();
		Time.timeScale = 1;
		GameMenu.Hide();
		ZoneMenu.Show ();
		SoundManager.Instance.RestartMainMenuSong ();
	}

	public void OnNextLevelPressed(){
		//load next level
		LevelSelectorManager.Instance.NextLevel ();
		Hide ();
	}
	public void OnResetPressed(){
		//load last level
		LevelSelectorManager.Instance.RestartLevel ();
		Hide ();
	}

}
