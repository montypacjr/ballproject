﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventMenu : Menu<EventMenu> {

	static Material exampleSkin;

	public Text nameText;

	void OnEnable(){
		SetName ();
	}

	public static void Show(Material unlockedSkin){
		exampleSkin = unlockedSkin;
		Open ();
	}

	public static void Hide(){
		Close ();
	}

	public void OnAcceptPressed(){
		Hide ();
		SkinManager.Instance.CheckForTrailUnlock (LevelSelectorManager.Instance.GetLevel ());
	}
	void SetName(){
		char[] separators = { '_' };
		string[] materialName = exampleSkin.name.Split (separators, 4);
		nameText.text = "";
		for (int i = 0; i < materialName.Length; i++) {
			if (materialName[i].Contains ("Player")) {
				continue;
			}
			nameText.text += materialName[i] + " ";
		}
	}
}
