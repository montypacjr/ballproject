﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathMenu : Menu<DeathMenu> {

	public static void Show()
	{
		Open();
		LevelSelectorManager.Instance.Disable ();
	}

	public static void Hide()
	{
		Close();
	}

	public void OnMainMenuPressed()
	{
		Hide();
		Time.timeScale = 1;
		GameMenu.Hide();
		ZoneMenu.Show ();
		SoundManager.Instance.RestartMainMenuSong ();
	}
	public void OnResetPressed(){
		//load last level
		LevelSelectorManager.Instance.RestartLevel ();
		Hide ();
	}
}
