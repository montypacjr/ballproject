﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinSelectorMenu : SimpleMenu<SkinSelectorMenu> {

	public void OnSkinPressed(){
		SkinMenu.Show ();
		SkinMenu.Instance.SetButtonText (GameManager.Instance.selector.lastText);
		GameManager.Instance.selector.EnableSkinSlector ();
	}

	public void OnTrailPressed(){
		TrailMenu.Show ();
		TrailMenu.Instance.SetButtonText (GameManager.Instance.selector.lastTextTrail);
		GameManager.Instance.selector.EnableTrailSlector ();
	}
	public override void OnBackPressed ()
	{
		GameManager.Instance.data.Save ();
		Hide ();
	}
}
