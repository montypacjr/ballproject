﻿using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : SimpleMenu<PauseMenu>
{
	public Image imagen;

	void OnEnable(){
		SetSound ();
	}

	public void OnMainMenuPressed()
	{
		Hide();
		Time.timeScale = 1;
		GameMenu.Hide();
		ZoneMenu.Show ();
		SoundManager.Instance.RestartMainMenuSong ();
		LevelSelectorManager.Instance.Disable ();
	}
	public override void OnBackPressed ()
	{
		Hide ();
		GameMenu.Instance.paused = false;
		Time.timeScale = 1;
	}
	public void OnSoundPressed(){
		if (MainMenu.playing) {
			MainMenu.playing = false;
			imagen.sprite = MainMenu.Instance.GetSoundOff ();
			SoundManager.Instance.Mute ();
		}else{
			MainMenu.playing = true;
			imagen.sprite = MainMenu.Instance.GetSoundOn ();
			SoundManager.Instance.UnMute ();
		}
	}
	void SetSound(){
		if (MainMenu.playing) {
			imagen.sprite = MainMenu.Instance.GetSoundOn ();
		}else{
			imagen.sprite = MainMenu.Instance.GetSoundOff ();
		}
	}
	public void OnResetPressed(){
		//load last level
		LevelSelectorManager.Instance.RestartLevel ();
		Time.timeScale = 1;
		Hide ();
	}
}
