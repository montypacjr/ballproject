﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StopMenu : SimpleMenu<StopMenu>
{
	public Transform buttonsLayout;
	string ringsLevel = "/6";

	void OnEnable(){
		UnlockButtons ();
		SetRings ();
	}

	void UnlockButtons(){
		int levelsToUnlock = GameManager.Instance.data.LevelsUnlocked - 29;
		for (int i = 0; i < buttonsLayout.childCount; i++) {
			buttonsLayout.GetChild (i).gameObject.GetComponent <Button>().interactable = false;
		}
		for (int i = 0; i < levelsToUnlock; i++) {
			buttonsLayout.GetChild (i).gameObject.GetComponent <Button>().interactable = true;
		}
	}
	void SetRings(){
		string ringNumber;
		for (int i = 0; i < 10; i++) {
			ringNumber = GameManager.Instance.GetLevelRings (3, i).ToString () + ringsLevel;
			buttonsLayout.GetChild (i).GetChild (0).gameObject.GetComponent <Text>().text = ringNumber;
		}
	}
	public void OnLevelPressed(int index){
		//load level
		LevelSelectorManager.Instance.SelectLevel (index,3);
		Hide ();
		GameMenu.Show ();

	}

	public override void OnBackPressed ()
	{
		Hide ();
		ZoneMenu.Show ();
	}
}

