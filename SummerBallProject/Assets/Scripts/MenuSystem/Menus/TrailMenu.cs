﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrailMenu : SimpleMenu<TrailMenu> {

	public Button button;
	public Image buttonImage;
	public Text text;
	public Sprite lockedImage;

	void OnEnable(){
		text.text = "u";
	}

	public void OnRightPressed(){
		//next trail
		GameManager.Instance.selector.NextTrailSkin ();
	}
	public void OnLeftPressed(){
		//previous trail
		GameManager.Instance.selector.PreviousTrailSkin ();
	}
	public void OnSelectPressed(){
		//selct trail
		if (text.text.Contains ("u")) {
			GameManager.Instance.selector.SelectTrail ();
			Hide ();
			TrailColorMenu.Show ();
		} else
			UnlockByLevelMenu.Show ();
		
	}

	public void SetButtonText(string text){
		if (text.Contains ("u")) {
			button.gameObject.SetActive (false);
			this.text.text = text;
		} else {
			button.gameObject.SetActive (true);
			button.interactable = false;
			buttonImage.sprite = lockedImage;
			this.text.gameObject.SetActive (true);
			this.text.text = text;
		}
	}
	public override void OnBackPressed ()
	{
		GameManager.Instance.selector.DisableAll ();
		Hide ();
	}

}
