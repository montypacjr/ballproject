﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcceptUnlockMenu : SimpleMenu<AcceptUnlockMenu> {

	public override void OnBackPressed ()
	{
		Hide ();
	}
	public void UnlockSkin(){
		GameManager.Instance.selector.UnlockSkin ();
	}
}
