﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameMenu : Menu<GameMenu>
{
	public bool paused = false;

	public Sprite ring,disabledRing;
	[SerializeField]
	Transform levelRingsCount;

	void OnEnable(){
		if (!paused) {
			ResetRings ();
			Time.timeScale = 1;
			GetComponent <Animation> ().Play ();
		} else
			paused = false;
	}
	void ResetRings(){
		for (int i = 0; i < levelRingsCount.childCount; i++) {
			levelRingsCount.GetChild (i).gameObject.GetComponent <Image> ().sprite = ring;
		}
	}
	public static void Show(){
		Open ();
	}
	public static void Hide(){
		Close ();
	}
	public void OnPausePressed(){
		paused = true;
		Time.timeScale = 0;
		PauseMenu.Show ();
	}
	public void OnRingPicked(int index){
		levelRingsCount.GetChild (index).gameObject.GetComponent <Image>().sprite = disabledRing;
	}
	public override void OnBackPressed()
	{
		PauseMenu.Show();
	}

}
