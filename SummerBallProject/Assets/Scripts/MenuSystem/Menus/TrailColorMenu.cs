﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailColorMenu : SimpleMenu<TrailColorMenu> {

	public void OnRightPressed(){
		//next color
		GameManager.Instance.selector.NextTrailColor ();
	}
	public void OnLeftPressed(){
		//previous color
		GameManager.Instance.selector.PreviousTrailColor ();
	}
	public void OnSelectPressed(){
		//select color
		Hide ();
		GameManager.Instance.selector.SelectTrailColor ();
	}
	public override void OnBackPressed ()
	{
		GameManager.Instance.selector.DisableAll ();
		Hide ();
	}
}
