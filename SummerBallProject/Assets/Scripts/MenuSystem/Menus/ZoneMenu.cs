﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ZoneMenu : SimpleMenu<ZoneMenu>
{
	public Transform buttonsLayout;
	public Text jumps, fast, revert, stop;
	string totalRingLevel = "/60";

	void OnEnable(){
		UnlockButtons ();
		SetZoneRings ();
	}

	void UnlockButtons(){
		int levelsToUnlock = (int)Math.Floor ((double)GameManager.Instance.data.LevelsUnlocked/10);
		for (int i = 0; i < buttonsLayout.childCount; i++) {
			buttonsLayout.GetChild (i).gameObject.GetComponent <Button>().interactable = false;
		}
		for (int i = 0; i <= levelsToUnlock; i++) {
			buttonsLayout.GetChild (i).gameObject.GetComponent <Button>().interactable = true;
		}
	}

	public void OnZoneSelected(int zone){
		Hide ();
		switch (zone) {
		case 0:
			JumpsMenu.Show ();
			break;
		case 1:
			AccelDeccelMenu.Show ();
			break;
		case 2:
			ChangeMenu.Show ();
			break;
		case 3:
			StopMenu.Show ();
			break;
		default:
			break;
		}
	}
	void SetZoneRings(){
		jumps.text = GameManager.Instance.data.TotalJumps.ToString () + totalRingLevel;
		fast.text = GameManager.Instance.data.TotalFast.ToString () + totalRingLevel;
		revert.text = GameManager.Instance.data.TotalRevert.ToString () + totalRingLevel;
		stop.text = GameManager.Instance.data.TotalStop.ToString () + totalRingLevel;
	}
}

