﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenu : SimpleMenu<MainMenu>
{
	public GameObject unlockLevelButton;
	public Image imagen;
	public Sprite soundOn, soundOff;
	public static bool playing = true;

	void OnEnable(){
		SetSound ();
		if (Application.isEditor||Debug.isDebugBuild) {
			unlockLevelButton.SetActive (true);
		}else{
			unlockLevelButton.SetActive (false);
		}
	}

	public void OnLevelPressed()
	{
		ZoneMenu.Show();
	}

	public void OnShopPressed()
	{
		SkinSelectorMenu.Show();
	}

	public void OnSoundPressed(){
		if (playing) {
			playing = false;
			imagen.sprite = soundOff;
			SoundManager.Instance.Mute ();
		}else{
			playing = true;
			imagen.sprite = soundOn;
			SoundManager.Instance.UnMute ();
		}
	}
	void SetSound(){
		if (playing) {
			imagen.sprite = soundOn;
		}else{
			imagen.sprite = soundOff;
		}
	}
	public Sprite GetSoundOff(){
		return soundOff;
	}
	public Sprite GetSoundOn(){
		return soundOn;
	}
    public void OnInfoPressed()
    {
		InfoMenu.Show();
	}
	public void OnAdLevelPressed(){
		if (GameManager.Instance.data.LevelsUnlocked < 39) {
			GameManager.Instance.UnlockLevel ();
		}else{
			unlockLevelButton.SetActive (false);
		}
	}
	public override void OnBackPressed()
	{
		GameManager.Instance.data.Save ();
		Application.Quit();
	}
}
