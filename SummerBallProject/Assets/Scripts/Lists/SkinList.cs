﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Security.Cryptography;

[CreateAssetMenu(menuName = "SkinList")]
public class SkinList : ScriptableObject{

	[Tooltip("Rings to unlock a skin.")]
	public int[] ringsPerSkin;

	public Material[] skinList;

	bool[] unlocked;

	void OnEnable(){
		unlocked = new bool[ringsPerSkin.Length];
		if (ringsPerSkin[0] == -1) {
			unlocked [0] = true;
		}
	}

	public Material GetSkin(int index){
		return skinList [index];
	}
	public Material GetRandomSkin(){
		return skinList [Random.Range (0, skinList.Length)];
	}
	public bool isSkinUnlocked(int index){
		return unlocked [index];
	}
	public int Length(){
		return skinList.Length;
	}
	public void UnlockSkin(int index){
		unlocked [index] = true;
	}
	public bool[] GetUnlocked(){
		return unlocked;
	}
	public void SetUnlocked(bool[] unlocked){
		this.unlocked = unlocked;
	}
}
