﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ColorList")]
public class ColorList : ScriptableObject {

	public Color[] colors;

	public Color GetColor(int index){
		return colors[index];
	}

	public int Length(){
		return colors.Length;
	}

}
