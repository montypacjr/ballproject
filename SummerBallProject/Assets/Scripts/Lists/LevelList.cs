﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Zone")]
public class LevelList : ScriptableObject {

	public Texture2D[] levels;

	public Texture2D GetLevel(int index){
		return levels [index];
	}

	public int Lenght(){
		return levels.Length;
	}


}

