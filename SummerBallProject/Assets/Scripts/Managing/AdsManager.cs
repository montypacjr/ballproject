﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdsManager : MonoBehaviour {
	
	public static AdsManager Instance;

	void Awake(){
		if(Instance == null){
			Instance = this;
		}else{
			Destroy (gameObject);
		}
	}

	public void ShowRewardedAd(){
		if (Advertisement.IsReady ("rewardedVideo")) {
			var options = new ShowOptions {resultCallback = HandleShowResult};
			Advertisement.Show ("rewardedVideo", options);
		}
	}
	private void HandleShowResult(ShowResult result){
		switch (result) {
		case ShowResult.Finished:
			SkinMenu.Instance.SetButtonText ("u");
			GameManager.Instance.selector.UnlockAdSkin ();
			break;
		default:
			AdWentWrongMenu.Show ();
			break;
		}
	}
}
