﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	public static SoundManager Instance;

	public AudioSource[] songs;

	public AudioSource accel, ring, button, changeDir, elevator, end, hit, doubleJ, baseJ, slow, start;

	void Awake(){
		if(Instance == null){
			Instance = this;
		}else{
			Destroy (gameObject);
		}
	}

	public void Mute(){
		for (int i = 0; i < transform.childCount; i++) {
			transform.GetChild (i).gameObject.GetComponent <AudioSource>().volume = 0;
		}
	}

	public void UnMute(){
		for (int i = 0; i < transform.childCount; i++) {
			transform.GetChild (i).gameObject.GetComponent <AudioSource>().volume = 0.5f;
		}
		for (int i = 0; i < songs.Length; i++) {
			songs [i].volume = 0.15f;
		}
	}

	public void RestartMainMenuSong(){
		for (int i = 0; i < songs.Length - 1; i++) {
			songs [i].Stop ();
		}
		songs [4].Play ();
	}

}
