﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectorManager : MonoBehaviour {

	public static LevelSelectorManager Instance;

	public LevelGenerator generator;

	public Transform[] levels;

	int lastLevel;
	int lastZone;

	void Awake(){
		if(Instance == null){
			Instance = this;
		}else{
			Destroy (gameObject);
		}
	}

	public void SelectLevel(int index, int zone){
		
		generator.GenerateLevel (index, zone);

		for (int i = 0; i < levels[zone].childCount; i++) {
			levels [zone].GetChild (i).gameObject.SetActive (false);
		}

		if(levels [zone]!=null)
			levels [zone].GetChild(index).gameObject.SetActive (true);
		//fade out to start

		lastLevel = index;
		lastZone = zone;
		SoundManager.Instance.songs [4].Stop ();
		SoundManager.Instance.songs [zone].Play ();
	}
	public void RestartLevel(){
		SelectLevel (lastLevel,lastZone);
	}
	public void NextLevel(){
		lastLevel++;
		if(lastLevel < 10)
			SelectLevel (lastLevel,lastZone);
	}
	public int GetLevel (){
		return lastLevel;
	}
	public int GetZone(){
		return lastZone;
	}

	public void Disable(){
		generator.ResetPools ();

		for (int i = 0; i < levels[lastZone].childCount; i++) {
			levels [lastZone].GetChild (i).gameObject.SetActive (false);
		}
	}

}
