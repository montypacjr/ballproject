﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public static GameManager Instance;

	public GameData data;

	public SkinSelector selector;
	[SerializeField]
	bool trailSelection;

	bool leftDir,updated;

	void Awake(){
		if(Instance == null){
			Instance = this;
		}else{
			Destroy (gameObject);
		}
		data.Load ();
	}

	void Start(){
	}

	public bool isLeft(){
		return leftDir;
	}

	public void setLeft(bool state){
		leftDir = state;
		updated = false;
	}

	public bool isUpdated(){
		return updated;
	}

	public void setUpdate(bool state){
		updated = state;
	}

	public void EndLevel(int ringNumber){
		if (!Application.isEditor && !Debug.isDebugBuild)
			data.LevelsUnlocked++;
		EndLevelMenu.Show (ringNumber);
		data.Save ();
	}

	public void Death(){
		DeathMenu.Show ();
	}
	public void UnlockLevel(){
		data.LevelsUnlocked++;
	}
	public void SetSkin(){
		selector.SetInitialPlayerSkin ();
	}

	public bool isTrailSelection(){
		return trailSelection;
	}

	public void SetTrailSelection(bool state){
		trailSelection = state;
	}

	public int GetLevelRings(int zone, int level){
		return data.GetLevelRings (zone, level);
	}

}
