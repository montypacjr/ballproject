﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiPool : MonoBehaviour {

	GenericPool[] pools;

	public GameObject[] pooledObjects;
	public int initialPooledAmount;
	public bool willGrow;

	void Awake() {
		pools = new GenericPool[pooledObjects.Length];
		for(int i = 0; i < pooledObjects.Length; i++){
			GameObject obj = new GameObject (pooledObjects[i].name);
			obj.transform.SetParent (transform);
			pools [i] = obj.AddComponent <GenericPool> ();
			pools [i].initialPooledAmount = initialPooledAmount;
			pools [i].willGrow = willGrow;
			pools [i].pooledObject = pooledObjects [i];
			pools [i].InitializePool ();
		}

	}
//	public GameObject GetPooledObject(int index){
//
//		return pools [index].GetPooledObject ();
//
//	}

	public GenericPool GetPool(int index){
		return pools [index];
	}
}
