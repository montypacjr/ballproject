﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Analytics;

#if UNITY_EDITOR

using UnityEditor;
using UnityEditorInternal;

[ExecuteInEditMode]
public class ColliderGenerator : MonoBehaviour {

	public Transform parent;

	public GameObject player;

	public GameObject[] prefabs;

	public GameObject deathZone;

	public Transform colliders;

	public PhysicMaterial noBounce;

	public Texture2D editorGeneratorLevel;

	int floorCount;

	public Color floor,enemy,ring,start,exit,accel,deccel,stop,elevator,change,djump,movingEnemy;

	public int rows,columns;

	public GameObject colliderAtachment;

	void Start(){
		floor = Color.black;
		enemy = Color.red;
		ring  = new Color (1,1,0,1);
		start = Color.green;
		exit = Color.magenta;
		accel = Color.blue;
		deccel = Color.cyan;
	}
	
	public void GenerateColliders(){

		Color temp;

		rows = editorGeneratorLevel.height;
		columns = editorGeneratorLevel.width;

		for (int r = 0; r < rows; r++) {
			for (int c = 0; c <= columns; c++) {

				temp = editorGeneratorLevel.GetPixel (c, r);

				if (temp == Color.white) {
					if (floorCount != 0) {
						NewCollider (floorCount, new Vector2 (c, r));
						floorCount = 0;
					}
				} else if (temp != enemy && temp != movingEnemy && temp != ring && temp != start) {
					floorCount++;
				} else {
					if (floorCount != 0) {
						NewCollider (floorCount, new Vector2 (c, r));
						floorCount = 0;
					}
				}
				if (c == columns) {
					if (floorCount != 0) {
						NewCollider (floorCount-1, new Vector2 (c, r));
						floorCount = 0;
					}
				}

			}
			floorCount = 0;
		}
	}

	void NewCollider(int xSize, Vector2 pos){
		GameObject go = new GameObject ();

		BoxCollider bc = go.AddComponent <BoxCollider>();

		if (xSize == 1) {
			go.name = StaticStrings.Tags.wallTag;
			bc.material = noBounce;
		}else {
			go.name = StaticStrings.Tags.floorTag;
		}

		bc.size = new Vector3 (xSize,1,1);

		if (xSize % 2 == 0) {
			bc.center = new Vector3 (pos.x - (float)(xSize / 2) - 0.5f, pos.y, 0);
		}else
			bc.center = new Vector3 (pos.x - 1 - (xSize / 2), pos.y, 0);

		go.transform.parent = colliders;

	}

	public void GenerateLevel(){

		rows = editorGeneratorLevel.width;
		columns = editorGeneratorLevel.height;

		Color temp;

		for(int r = 0; r < rows; r++){

			for(int c = 0; c < columns; c++){
				temp = editorGeneratorLevel.GetPixel (r, c);

				if (temp == floor) {
					InstancePrefab (0, new Vector3 (r, c, 0),0);
				}
				if (temp == enemy) {
					InstancePrefab (1, new Vector3 (r, c-0.5f, 0),0);
				}
				if (temp == ring) {
					InstancePrefab (2, new Vector3 (r, c, 0),0);
				}
				if (temp == start) {
					player.transform.position = new Vector3 (r, c, 0);
				}
				if (temp == exit) {
					InstancePrefab (3, new Vector3 (r, c, 0),0);
				}
				if (temp == accel) {
					InstancePrefab (4, new Vector3 (r, c, 0),0);
				}
				if (temp == deccel) {
					InstancePrefab (5, new Vector3 (r, c, 0),0);
				}
				if (temp == stop) {
					InstancePrefab (6, new Vector3 (r, c, 0),0);
				}
				if (temp == elevator) {
					InstancePrefab (7, new Vector3 (r, c, 0),0);
				}
				if (temp == change) {
					InstancePrefab (8, new Vector3 (r, c, 0),0);
				}
				if (temp == djump) {
					InstancePrefab (9, new Vector3 (r, c, 0),0);
				}
				if (temp == movingEnemy) {
					InstancePrefab (10, new Vector3 (r, c-0.5f, 0),0);
				}
			}
		}
	}

	void InstancePrefab(int index, Vector3 position,int extra){
		Debug.Log ("Generated: "+ index.ToString () + " at position: " + position.ToString ());
		GameObject.Instantiate (prefabs[index],position,Quaternion.identity,parent);
	}

	public void AttachColliders(){

		colliderAtachment = GameObject.Find (editorGeneratorLevel.name);

		if (colliderAtachment == null) {
			colliderAtachment = new GameObject (editorGeneratorLevel.name);
		}

		if (colliderAtachment.transform.childCount != 0) {
			var temp = colliderAtachment.transform.Cast<Transform> ().ToList ();
			foreach (var child in temp) {
				if (!child.name.Contains ("UpwardsFan")) {
					DestroyImmediate (child.gameObject);
				}
			}
		}

		GameObject go;

		GameObject wall = new GameObject ("WallColliders");
		GameObject floor = new GameObject ("FloorColliders");

		wall.tag = StaticStrings.Tags.wallTag;
		floor.tag = StaticStrings.Tags.floorTag;

		for (int i = 0; i < colliders.childCount; i++) {
			go = colliders.GetChild (i).gameObject;

			ComponentUtility.CopyComponent (go.GetComponent <BoxCollider>());

			if (go.name == StaticStrings.Tags.floorTag) {
				
				ComponentUtility.PasteComponentAsNew(floor);

			}else if (go.name == StaticStrings.Tags.wallTag) {

				ComponentUtility.PasteComponentAsNew(wall);

			}

		}

		wall.transform.parent = colliderAtachment.transform;
		floor.transform.parent = colliderAtachment.transform;

		go = Instantiate (deathZone,new Vector3(0,-1,0),Quaternion.identity,colliderAtachment.transform);
	}

	public void Clean(){
		if (colliders.childCount != 0) {
			var temp = colliders.Cast<Transform> ().ToList ();
			foreach (var child in temp) {
				DestroyImmediate (child.gameObject);
			}
		}
		if (parent.childCount != 0) {
			var temp1 = parent.Cast<Transform> ().ToList ();
			foreach (var child in temp1) {
				DestroyImmediate (child.gameObject);
			}
		}
		player.transform.position = new Vector3 (0, 3, 0);
	}

}
#endif
