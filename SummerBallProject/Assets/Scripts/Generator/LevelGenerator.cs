﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour {

	GenericPool[] pools;

	public Transform parent;

	public GameObject player;

	public Texture2D[] level;

	public LevelList[] zones;

	public GameObject[] prefabs;

	public int rows,columns;

	public Color floor,enemy,ring,start,exit,accel,deccel,stop,elevator,change,djump,movingEnemy;

	void Start(){
		floor = Color.black;
		enemy = Color.red;
		ring  = new Color (1,1,0,1);
		start = Color.green;
		exit = Color.magenta;
		accel = Color.blue;
		deccel = Color.cyan;
		pools = new GenericPool[prefabs.Length];
		InitializePools ();
	}


	public void GenerateLevel(int index, int zone){
		ResetPools ();

		if (zone < zones.Length && zones [zone] != null && index < zones [zone].Lenght () && zones [zone].GetLevel (index) != null) {
			Color temp;
			Texture2D tex = zones [zone].GetLevel (index);
			//rows = level [index].width;
			//columns = level [index].height;
			rows = tex.width;
			columns = tex.height;

			for(int r = 0; r < rows; r++){

				for(int c = 0; c < columns; c++){
					temp = tex.GetPixel (r, c);

					if (temp == floor) {
						InstancePrefab (0, new Vector3 (r, c, 0));
					}
					if (temp == enemy) {
						InstancePrefab (1, new Vector3 (r, c-0.5f, 0));
					}
					if (temp == ring) {
						InstancePrefab (2, new Vector3 (r, c, 0));
					}
					if (temp == start) {
						player.transform.position = new Vector3 (r, c, 0);
						player.transform.GetComponentInChildren <TrailRenderer>().Clear ();
						player.SetActive (true);
						ParticleManager.Instance.transform.position = new Vector3 (r, c - 3, 0);
					}
					if (temp == exit) {
						InstancePrefab (3, new Vector3 (r, c, 0));
					}
					if (temp == accel) {
						InstancePrefab (4, new Vector3 (r, c, 0));
					}
					if (temp == deccel) {
						InstancePrefab (5, new Vector3 (r, c, 0));
					}
					if (temp == stop) {
						InstancePrefab (6, new Vector3 (r, c, 0));
					}
					if (temp == elevator) {
						InstancePrefab (7, new Vector3 (r, c, 0));
					}
					if (temp == change) {
						InstancePrefab (8, new Vector3 (r, c, 0));
					}
					if (temp == djump) {
						InstancePrefab (9, new Vector3 (r, c, 0));
					}
					if (temp == movingEnemy) {
						InstancePrefab (10, new Vector3 (r, c-0.5f, 0));
					}
				}
			}
		}

	}

	void InstancePrefab(int index, Vector3 position){
		//Debug.Log ("Generated: "+ index.ToString () + " at position: " + position.ToString ());
		GameObject go = pools[index].GetPooledObject ();
		go.transform.position = position;
		go.SetActive (true);
	}

	void InitializePools(){
		pools [0] = transform.GetChild (0).GetComponent <GenericPool>();
		pools [1] = transform.GetChild (1).GetComponent <MultiPool>().GetPool (0);
		pools [10] = transform.GetChild (1).GetComponent <MultiPool>().GetPool (1);
		pools [7] = transform.GetChild (2).GetComponent <GenericPool>();
		pools [2] = transform.GetChild (3).GetComponent <MultiPool>().GetPool (0);
		pools [3] = transform.GetChild (3).GetComponent <MultiPool>().GetPool (1);
		pools [4] = transform.GetChild (3).GetComponent <MultiPool>().GetPool (2);
		pools [5] = transform.GetChild (3).GetComponent <MultiPool>().GetPool (3);
		pools [6] = transform.GetChild (3).GetComponent <MultiPool>().GetPool (4);
		pools [8] = transform.GetChild (3).GetComponent <MultiPool>().GetPool (5);
		pools [9] = transform.GetChild (3).GetComponent <MultiPool>().GetPool (6);
	}

	public void ResetPools(){
		for (int i = 0; i < pools.Length; i++) {
			pools [i].ResetPool ();
		}
		player.SetActive (false);
	}


}
