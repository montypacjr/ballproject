﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

	public Transform target;

	Vector3 initialPos;

	public float lerpValue;

	float offsetZ,offsetY;

	void Awake(){
		offsetZ = transform.position.z;
		offsetY = transform.position.y;
		initialPos = transform.position;
	}

	void Update(){
		if (GameManager.Instance.isTrailSelection ())
			transform.Translate (Vector3.right * Time.deltaTime * 8);
		if(target.gameObject.activeSelf && !GameManager.Instance.isTrailSelection ())
			transform.position = Vector3.Lerp (transform.position,new Vector3(target.position.x,target.position.y + offsetY - 1,target.position.z + offsetZ),lerpValue);
		if (!target.gameObject.activeSelf && !GameManager.Instance.isTrailSelection ())
			transform.position = initialPos;

	}

}
