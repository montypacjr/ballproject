﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour {

	public bool center;

	public float roomSize;

	public float width, height;

	float negativeWidthOffset, negativeHeightOffset;

	void OnDrawGizmos(){
		Gizmos.color = Color.black;
		if (center) {
			negativeWidthOffset = width / 2;
			negativeHeightOffset = height / 2;
		}else {
			negativeWidthOffset = 0;
			negativeHeightOffset = 0;
		}
		if (width > 0 && height > 0 && roomSize > 0)
		{
			for (float x = 0; x <= width * roomSize; x += roomSize)
				Gizmos.DrawLine(new Vector3(x - (negativeWidthOffset + 0.5f),-(negativeHeightOffset + 0.5f), 0), new Vector3(x - (negativeWidthOffset + 0.5f), (height * roomSize) - (negativeHeightOffset + 0.5f), 0));
			for (float z = 0; z <= height * roomSize; z += roomSize)
				Gizmos.DrawLine(new Vector3(-(negativeWidthOffset + 0.5f), z - (negativeHeightOffset + 0.5f), 0), new Vector3((width * roomSize) - (negativeWidthOffset + 0.5f), z - (negativeHeightOffset + 0.5f), 0));
		}
	}
}
