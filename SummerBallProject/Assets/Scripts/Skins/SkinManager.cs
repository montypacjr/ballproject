﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinManager : MonoBehaviour {

	public SkinList list;

	public SkinList trailList;

	public static SkinManager Instance;

	void Awake(){
		if(Instance == null){
			Instance = this;
		}else{
			Destroy (gameObject);
		}
	}

	public void CheckForSkinUnlock(int lastLevel){

		lastLevel++;

		for (int i = 1; i < list.Length (); i++) {
			if (list.ringsPerSkin[i] == lastLevel && !list.isSkinUnlocked (i)) {
				EventMenu.Show (list.GetSkin (i));
				list.UnlockSkin (i);
				GameManager.Instance.data.Save ();
				break;
			}
		}

	}
	public void CheckForTrailUnlock(int lastLevel){

		lastLevel++;

		for (int i = 1; i < trailList.Length (); i++) {
			if (trailList.ringsPerSkin[i] == lastLevel && !trailList.isSkinUnlocked (i)) {
				EventMenu.Show (trailList.GetSkin (i));
				trailList.UnlockSkin (i);
				GameManager.Instance.data.Save ();
				break;
			}
		}

	}

}
