﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Security.Cryptography;

public class SkinSelector : MonoBehaviour {

	public SkinList baseList,normalList,rareList;

	public SkinList trailList;

	public ColorList trailColors;

	public GameObject player;

	public GameObject skinSelector;

	public GameObject trailSelector;

	public Transform trail;

	public string lastText;
	public string lastTextTrail;

	int actualIndex,actualTrailIndex,actualTrailColorIndex;
	string level = "Lvl. ";
	string ring = "Rings. ";
	string ad = "ad";
	string unlocked = "u";

	void Start(){
		lastText = "u";
		lastTextTrail = "u";
		LoadSkins ();
		player.SetActive (false);
		skinSelector.GetComponent <MeshRenderer> ().material = GetSkin (actualIndex);
		trailSelector.GetComponent <MeshRenderer> ().material = GetSkin (actualIndex);
		trail.GetComponent <TrailRenderer> ().material = trailList.GetSkin (actualTrailIndex);
		trail.GetComponent <TrailRenderer> ().startColor = trailColors.GetColor (actualTrailColorIndex);
	}
	public void NextTrailSkin(){
		if (actualTrailIndex + 1 == trailList.Length ()) {
			trail.GetComponent <TrailRenderer> ().material = trailList.GetSkin (0);
			actualTrailIndex = 0;
		}
		else 
			trail.GetComponent <TrailRenderer> ().material = trailList.GetSkin (++actualTrailIndex);

		SetUnlockButtonTrail (actualTrailIndex);
	}
	public void NextTrailColor(){
		if (actualTrailColorIndex + 1 == trailColors.Length ()) {
			trail.GetComponent <TrailRenderer> ().startColor = trailColors.GetColor (0);
			actualTrailColorIndex = 0;
		}
		else 
			trail.GetComponent <TrailRenderer> ().startColor = trailColors.GetColor (++actualTrailColorIndex);
	}
	public void NextSkin(){
		if (actualIndex + 1 == MaxSkin ()) {
			skinSelector.GetComponent <MeshRenderer> ().material = GetSkin (0);
			actualIndex = 0;
		}
		else 
			skinSelector.GetComponent <MeshRenderer>().material = GetSkin (++actualIndex);

		SetUnlockButtonSkin (actualIndex);
	}
	public void PreviousTrailSkin(){
		if (actualTrailIndex - 1 < 0) {
			trail.GetComponent <TrailRenderer> ().material = trailList.GetSkin (trailList.Length () - 1);
			actualTrailIndex = trailList.Length () - 1;
		}
		else 
			trail.GetComponent <TrailRenderer> ().material = trailList.GetSkin (--actualTrailIndex);

		SetUnlockButtonTrail (actualTrailIndex);
	}
	public void PreviousTrailColor(){
		if (actualTrailColorIndex - 1 < 0) {
			trail.GetComponent <TrailRenderer> ().startColor = trailColors.GetColor (trailColors.Length () - 1);
			actualTrailColorIndex = trailColors.Length () - 1;
		}
		else 
			trail.GetComponent <TrailRenderer> ().startColor = trailColors.GetColor (--actualTrailColorIndex);
	}
	public void PreviousSkin(){
		if (actualIndex - 1 < 0) {
			skinSelector.GetComponent <MeshRenderer> ().material = GetSkin (MaxSkin () - 1);
			actualIndex = MaxSkin () - 1;
		}
		else 
			skinSelector.GetComponent <MeshRenderer>().material = GetSkin (--actualIndex);

		SetUnlockButtonSkin (actualIndex);
	}

	public void SelectSkin(){
		if (isUnlocked (actualIndex)) {
			player.GetComponent <MeshRenderer>().material = GetSkin (actualIndex);
			trailSelector.GetComponent <MeshRenderer> ().material = GetSkin (actualIndex);
			skinSelector.GetComponent <MeshRenderer> ().material = GetSkin (actualIndex);
			GameManager.Instance.data.Skin = actualIndex;
			DisableAll ();
		}
	}

	Material GetSkin(int index){
		if (index < baseList.Length ()) {
			return baseList.GetSkin (index);
		}else if (index - baseList.Length () < normalList.Length ()) {
			return normalList.GetSkin (index - baseList.Length ());
		}else{
			return rareList.GetSkin (index - baseList.Length () - normalList.Length ());
		}
	}
	int MaxSkin(){
		int count = baseList.Length () + normalList.Length () + rareList.Length ();
		return count;
	}

	bool isUnlocked(int index){
		if (index < baseList.Length ()) {
			return baseList.isSkinUnlocked (index);
		}else if (index - baseList.Length () < normalList.Length ()) {
			return normalList.isSkinUnlocked(index - baseList.Length ());
		}else{
			return rareList.isSkinUnlocked(index - baseList.Length () - normalList.Length ());
		}
	}

	public void UnlockSkin(){
		if (actualIndex - baseList.Length () < normalList.Length ()) {
			if (GameManager.Instance.data.TotalRings >= normalList.ringsPerSkin[actualIndex - baseList.Length ()]) {
				normalList.UnlockSkin(actualIndex - baseList.Length ());
				GameManager.Instance.data.Save ();
				SkinMenu.Instance.SetButtonText (unlocked);
				lastText = unlocked;
				AcceptUnlockMenu.Hide ();
			}else{
				InsufficentRingsMenu.Show ();
			}
		}else{
			return;
		}
	}
	public void UnlockAdSkin(){
		if (actualIndex - baseList.Length () - normalList.Length () < rareList.Length ()) {
			rareList.UnlockSkin (actualIndex - baseList.Length () - normalList.Length ());
			GameManager.Instance.data.Save ();
		}
	}

	public void SetUnlockButtonSkin(int index){

		if (index < baseList.Length ()) {
			
			if (!baseList.isSkinUnlocked(index) && SkinMenu.Instance != null) {
				SkinMenu.Instance.SetButtonText (level + baseList.ringsPerSkin[index].ToString ());
				lastText = level + baseList.ringsPerSkin [index].ToString ();
			}else{
				SkinMenu.Instance.SetButtonText (unlocked);
				lastText = unlocked;
			}

		}else if (index - baseList.Length () < normalList.Length ()) {
			
			if (!normalList.isSkinUnlocked(index - baseList.Length ()) && SkinMenu.Instance != null) {
				SkinMenu.Instance.SetButtonText (ring + normalList.ringsPerSkin[index - baseList.Length ()].ToString ());
				lastText = ring + normalList.ringsPerSkin [index - baseList.Length ()].ToString ();
			}else{
				SkinMenu.Instance.SetButtonText (unlocked);
				lastText = unlocked;
			}

		}else{
			
			if (!rareList.isSkinUnlocked (index - baseList.Length () - normalList.Length ()) && SkinMenu.Instance != null) {
				SkinMenu.Instance.SetButtonText (ad);
				lastText = ad;
			}else{
				SkinMenu.Instance.SetButtonText (unlocked);
				lastText = unlocked;
			}

		}

	}
	public void SetUnlockButtonTrail(int index){
		if (!trailList.isSkinUnlocked (index) && TrailMenu.Instance != null) {
			TrailMenu.Instance.SetButtonText (level + trailList.ringsPerSkin [index].ToString ());
			lastTextTrail = level + trailList.ringsPerSkin [index].ToString ();
		} else { 
			TrailMenu.Instance.SetButtonText (unlocked);
			lastTextTrail = unlocked;
		}
		
	}
	public void SelectTrail(){
		if (trailList.isSkinUnlocked (actualTrailIndex)) {
			GameManager.Instance.data.TrailSkin = actualTrailIndex;
		}else{
			trail.GetComponent <TrailRenderer> ().material = trailList.GetSkin (GameManager.Instance.data.TrailSkin);
		}
	}

	public void SelectTrailColor(){
		GameManager.Instance.data.TrailColor = actualTrailColorIndex;
		DisableAll ();
	}

	public void EnablePlayer(){
		player.SetActive (true);
		skinSelector.SetActive (false);
		trailSelector.SetActive (false);
		StopTrail ();
	}
	public void DisableAll(){
		player.SetActive (false);
		skinSelector.SetActive (false);
		trailSelector.SetActive (false);
		StopTrail ();
	}
	public void EnableSkinSlector(){
		player.SetActive (false);
		skinSelector.SetActive (true);
		trailSelector.SetActive (false);
		StopTrail ();
	}
	public void EnableTrailSlector(){
		player.SetActive (false);
		skinSelector.SetActive (false);
		trailSelector.SetActive (true);
		MoveTrail ();
	}
	public void SetInitialPlayerSkin(){
		actualIndex = GameManager.Instance.data.Skin;
		player.GetComponent <MeshRenderer>().material = GetSkin (actualIndex);
		trailSelector.GetComponent <MeshRenderer> ().material = GetSkin (actualIndex);
		skinSelector.GetComponent <MeshRenderer> ().material = GetSkin (actualIndex);
	}

	void StopTrail(){
		GameManager.Instance.SetTrailSelection (false);
		MoveTrailToPlayer ();
	}

	void MoveTrail(){
		MoveTrailToSelector ();
		GameManager.Instance.SetTrailSelection (true);
	}

	public void MoveTrailToPlayer(){
		trail.SetParent (player.transform);
		trail.localPosition = Vector3.zero;
	}

	public void MoveTrailToSelector(){
		trail.SetParent (trailSelector.transform);
		trail.GetComponent <TrailRenderer>().Clear ();
		trail.localPosition = Vector3.zero;
	}

	public void LoadSkins(){
		actualIndex = GameManager.Instance.data.Skin;
		actualTrailIndex = GameManager.Instance.data.TrailSkin;
		actualTrailColorIndex = GameManager.Instance.data.TrailColor;
	}

}
