﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	public delegate void UpdateStatus(bool state);
	public static event UpdateStatus left;

	public float speed;

	public float jump;

	Rigidbody myRbd;
	bool jumping;
	bool grounded;
	bool floating;
	bool stopped;
	bool doubleJump;
	bool dead,ended;

	enum Direction {Left,Right}
	Direction dir = Direction.Right;
	float directionValue;

	float baseSpeed;
	public int totalRings;
	public int levelRings;

	void Awake(){
		//totalRings = PlayerPrefs.GetInt (StaticStrings.Keys.totalRingKey);
		totalRings = GameManager.Instance.data.TotalRings;
		myRbd = GetComponent <Rigidbody>();
		baseSpeed = speed;
		GameManager.Instance.SetSkin ();
	}
	void OnEnable(){
		resetPlayer ();
	}

	void FixedUpdate(){
		if(!floating && grounded && !jumping && !stopped){
			myRbd.velocity = new Vector3(speed * directionValue,myRbd.velocity.y,0);
		}
	}

	void Update(){
		directionValue = dir == Direction.Left ? -1 : 1;
		if(Input.GetButtonDown ("Jump") || Input.GetMouseButtonDown (0)){
			
			GameObject go = EventSystem.current.currentSelectedGameObject;

			if (go == null) {
				
				if (!myRbd.isKinematic) {
					if (!stopped && !jumping && grounded && !floating) {
						if (doubleJump) {
							myRbd.AddForce (new Vector3 (0, jump * 2, 0), ForceMode.Impulse);
							SoundManager.Instance.doubleJ.Play ();
						} else {
							myRbd.AddForce (new Vector3 (0, jump, 0), ForceMode.Impulse);
							SoundManager.Instance.baseJ.Play ();
						}

						jumping = true;
						grounded = false;
					} else {
						stopped = false;
					}
				} else {
					myRbd.isKinematic = false;
					ParticleManager.Instance.startParticle.Play ();
					SoundManager.Instance.start.Play ();
				}
			}
		}
	}

	void OnCollisionEnter(Collision other){
		if (other.transform.tag == StaticStrings.Tags.wallTag) {
			myRbd.velocity = new Vector3(0,myRbd.velocity.y,0);
			myRbd.angularVelocity = Vector3.zero;
			jumping = false;
			grounded = false;
		}
		if(other.transform.tag == StaticStrings.Tags.floorTag && !grounded && !stopped){
			if(transform.position.y < 0.9f){
				ParticleManager.Instance.transform.position = transform.position;
				ParticleManager.Instance.deathParticle.Play ();
				SoundManager.Instance.hit.Play ();
				StartCoroutine (DeathParticleTimer (0.4f));
			}
			jumping = false;
			grounded = true;
		}
		if(other.transform.tag == StaticStrings.Tags.deathTag || other.transform.tag == StaticStrings.Tags.killingObjectTag){
			if (!dead) {
				ParticleManager.Instance.transform.position = transform.position;
				ParticleManager.Instance.deathParticle.Play ();
				SoundManager.Instance.hit.Play ();
				StartCoroutine (DeathParticleTimer (0.3f));
			}

		}
	}
//	void OnCollisionStay(Collision other){
//		if(other.transform.tag == StaticStrings.Tags.floorTag && !grounded && !stopped){
//			jumping = false;
//			grounded = true;
//		}
//	}
	void OnTriggerEnter(Collider other){
		if (other.transform.tag == StaticStrings.Tags.doubleJumpTag) 
			doubleJump = true;
		
		if(other.transform.tag == StaticStrings.Tags.accelerationPadTag){
			SoundManager.Instance.accel.Play ();
			speed *= 1.6f;
		}
		if(other.transform.tag == StaticStrings.Tags.breakPadTag){
			speed = baseSpeed;
			SoundManager.Instance.slow.Play ();
		}
		if(other.transform.tag == StaticStrings.Tags.nextLevelPadTag){
			//load next level
			if (!ended) {
				other.transform.GetChild (1).gameObject.GetComponent <ParticleSystem> ().Play ();
				SoundManager.Instance.end.Play ();
				StartCoroutine (EndParticleTimer (1f));
			}
		}
		if(other.transform.tag == StaticStrings.Tags.changeDirectionPadTag){
			SoundManager.Instance.changeDir.Play ();
			dir = dir == Direction.Left ? Direction.Right : Direction.Left;
			left (dir == Direction.Left);
			//grounded = true;
		}
		if (other.transform.tag == StaticStrings.Tags.stopPadTag){
			myRbd.velocity = Vector3.zero;
			myRbd.angularVelocity = Vector3.zero;
			stopped = true;
			jumping = false;
			grounded = true;
		}
		if (other.tag == StaticStrings.Tags.upperLevelPad) {
			SoundManager.Instance.elevator.Play ();
			jumping = false;
			grounded = true;
		}
		if(other.tag == StaticStrings.Tags.ringTag){
//			PlayerPrefs.SetInt (StaticStrings.Keys.totalRingKey,++totalRings);
			if (GameMenu.Instance != null) {
				GameMenu.Instance.OnRingPicked (levelRings++);
			}
			SoundManager.Instance.ring.Play ();
			other.transform.parent.gameObject.SetActive (false);
		}
	}
	void OnTriggerStay(Collider other){
		if (other.tag == StaticStrings.Tags.changeDirectionPadTag && myRbd.velocity == Vector3.zero) {
			myRbd.velocity = new Vector3 (speed * directionValue, 0, 0);
			jumping = false;
			grounded = true;
		}
		if(other.tag == StaticStrings.Tags.upperLevelPad){
			floating = true;
			myRbd.velocity = new Vector3 (0, baseSpeed * 0.5f, 0);
		}
	}
	void OnTriggerExit(Collider other){
		if(other.tag == StaticStrings.Tags.upperLevelPad){
			SoundManager.Instance.elevator.Pause ();
			dir = dir == Direction.Left ? Direction.Right : Direction.Left;
			left (dir == Direction.Left);
			floating = false;
		}
		if (other.transform.tag == StaticStrings.Tags.doubleJumpTag)
			doubleJump = false;
	}
	void resetPlayer(){
		levelRings = 0;
		myRbd.velocity = Vector3.zero;
		myRbd.rotation = Quaternion.identity;
		myRbd.isKinematic = true;
		grounded = false;
		dead = false;
		ended = false;
		speed = baseSpeed;
		dir = Direction.Right;
	}
	IEnumerator DeathParticleTimer(float seconds){
		dead = true;
		myRbd.velocity = Vector3.zero;
		yield return new WaitForSeconds (seconds);
		GameManager.Instance.Death ();
	}
	IEnumerator EndParticleTimer(float seconds){
		ended = true;
		myRbd.velocity = Vector3.zero;
		GameManager.Instance.data.SetLevelRings (LevelSelectorManager.Instance.GetZone (),LevelSelectorManager.Instance.GetLevel (),levelRings);
		if (GameManager.Instance.data.LevelsUnlocked < 39) {
			GameManager.Instance.data.LevelsUnlocked++;
		}
		yield return new WaitForSeconds (seconds);
		GameManager.Instance.EndLevel (levelRings);
		Time.timeScale = 0;
		resetPlayer ();
	}
}
